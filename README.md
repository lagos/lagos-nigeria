**Scope of Work**

This Data collection exercise was conducted by CPCS Transcom Ltd, on behald of Lagos Metropolitan Area Transport Authority supported by AFD.

In summary the data contains:
1. Bus stops and attribute information (Frequencies, stop times, shapes)
2. Identified bus routes and bus service types (Bus lines,  calendar and informal scheduling information )

This data is  visualized and is available for downloading through : [cpcslabs.ca](url)

**Authority of data production**

CPCS Transcom Limited (CPCS) has been engaged by the Lagos Metropolitan Area Transport Authority (LAMATA) to provide Consultancy Services for the Bus Industry Transition Program (BITP). In this project, CPCS collected data through implementation of surveys and manual traffic counts including : Bus route identification survey, Manual classified traffic counts, Bus lines identification survey, Boarding and alighting survey, Stakeholder survey of drivers, Stakeholder survey of bus owners, Stakeholder survey of ancilliary service providers, Onboard OD passenger survey, Passenger quality survey / Passenger mystery survey and intermodal hub identification survey. This data was collected to better understand the existing bus operators and their routes into the eight future Quality Bus Corridors (QBCs). 

**Geographic  Scope **

The geographic area of focus of this data is along eight proposed QBCs within the mainland of Lagos State. The majority of the corridors can be described as feeder routes that enable residents to move between communities and access intermodal hubs. The QBCs namely : 
1. Ojuelegba – Idi Araba – Ilasamaja 
2.	Iju Ishaga-Abule Egba
3.	Iyana Iba-Igando
4.	Ketu-Alapere-Akanimodo
5.	Onipanu-Oshodi
6.	Iyana Ipaja-Ayobo
7.	Yaba-Cele
8.	Anthony-Oshodi  :::::are strategically located to connect passengers with other transportation modalities, such as BRT, LRT, First-and-Last-Mile buses, and other paratransit services. The corridors vary significantly in length from 1.6 km to 14.4 km. All of these corridors currently host paratransit bus services with either danfo (14 pax capacity) or korope (7 pax capacity) vehicles. 

**Licensing**

Data is licensed under Open Data Commons Open Database License (ODbL). 

**Authorship and Acknowledgement**

Data collected by CPCS Transcom, Geotrans Ltd and Lagos Metropolitan Area Transport Authority.

**Data Status**

This is data is up to date as of December 2023.


